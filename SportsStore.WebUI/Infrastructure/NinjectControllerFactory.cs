﻿using System;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Ninject;
using SportsStore.Domain.Abstract;
using SportsStore.Domain.Concrete;
using SportsStore.WebUI.Infrastructure.Abstract;
using SportsStore.WebUI.Infrastructure.Concrete;

namespace SportsStore.WebUI.Infrastructure
{
    public class NinjectControllerFactory : DefaultControllerFactory
    {
        private IKernel ninjectKernel;

        public NinjectControllerFactory()
        {
            ninjectKernel = new StandardKernel();
            AddBindings();
        }

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            return controllerType == null
                ? null
                : (IController)ninjectKernel.Get(controllerType);
        }

        private void AddBindings()
        {
            ninjectKernel.Bind<IProductRepository>().To<EFProductRepository>();
            AddOrderProcessorBinding();
            ninjectKernel.Bind<IAuthProvider>().To<FormsAuthProvider>();
        }

        private void AddOrderProcessorBinding()
        {
            string folderToSaveEmails = HttpContext.Current.Server.MapPath("~/App_Data/OrderEmails");
            if (!Directory.Exists(folderToSaveEmails))
            {
                Directory.CreateDirectory(folderToSaveEmails);
            }

            var emailSetting = new EmailSettings
            {
                WriteAsFile = true,
                FileLocation = folderToSaveEmails
            };

            ninjectKernel.Bind<IOrderProcessor>()
                .To<EmailOrderProcessor>().WithConstructorArgument("settings", emailSetting);
        }
    }
}