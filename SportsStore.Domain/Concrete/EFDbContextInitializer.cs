﻿using System.Collections.Generic;
using System.Data.Entity;
using SportsStore.Domain.Entities;

namespace SportsStore.Domain.Concrete
{
    internal class EFDbContextInitializer : CreateDatabaseIfNotExists<EFDbContext>
    {
        protected override void Seed(EFDbContext context)
        {
            context.Products.AddRange(new List<Product> {
                new Product() { Name = "Kayak", Category="Watersports", Price = 275m, Description="A boat for one person"},
                new Product() { Name = "Lifejacket", Category="Watersports", Price = 48.95m, Description="Protective and fashionable"},
                new Product() { Name = "Soccer ball", Category="Soccer", Price = 19.50m, Description="FIFA-approved size and weight"},
                new Product() { Name = "Corner flags", Category="Soccer", Price = 34.95m, Description="Give your playing field that proffesional touch"},
                new Product() { Name = "Stadium", Category="Soccer", Price = 79500m, Description="Flat-packed 35,000-seat stadium	Soccer"},
                new Product() { Name = "Thinking cap", Category="Chess", Price = 16m, Description="Improve your brain efficiency by 75%"},
                new Product() { Name = "Unsteady chair", Category="Chess", Price = 29.95m, Description="Secretly give your opponent a disadvantage"},
                new Product() { Name = "Human Chess Board", Category="Chess", Price = 75m, Description="A fun game for whole family"},
                new Product() { Name = "Bling-bling King", Category="Chess", Price = 1200m, Description="Gold-played, diamond-studded King"},
            });

            context.SaveChanges();
        }
    }
}
