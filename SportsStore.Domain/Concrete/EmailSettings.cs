﻿namespace SportsStore.Domain.Concrete
{
    public class EmailSettings
    {
        public string MailToAddress { get; set; } = "orders@example.com";

        public string MailFromAddress { get; set; } = "sportsstore@example.com";

        public bool UseSsl { get; set; } = true;

        public string Username { get; set; } = "MySmtpUsername";

        public string Password { get; set; } = "MySmtpPassword";

        public string ServerName { get; set; } = "smtp.example.com";

        public int ServerPort { get; set; } = 587;

        public bool WriteAsFile { get; set; }

        public string FileLocation { get; set; }
    }
}
