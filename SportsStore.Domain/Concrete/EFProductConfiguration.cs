﻿using System.Data.Entity.ModelConfiguration;
using SportsStore.Domain.Entities;

namespace SportsStore.Domain.Concrete
{
    internal class EFProductConfiguration : EntityTypeConfiguration<Product>
    {
        public EFProductConfiguration()
        {
            Property(e => e.Name).IsRequired().HasMaxLength(100);

            Property(e => e.Description).IsRequired().HasMaxLength(500);

            Property(e => e.Category).IsRequired().HasMaxLength(50);

            Property(e => e.Price).IsRequired().HasPrecision(16, 2);

            Property(e => e.ImageMimeType).HasMaxLength(50);
        }
    }
}
