﻿using System.Data.Entity.SqlServer;

namespace SportsStore.Domain
{
    // Code in this class never actually gets executed, see comments for EnsureStaticReference<T>() for details.
    internal class MissingDllHack
    {
        private static void EnsureStaticReferences()
        {
            // EntityFramework.SqlServer.dll
            EnsureStaticReference<SqlProviderServices>();
        }

        // Ensures that required assembly will be copied to output folder of referencing projects
        // without requiring a direct dependency on the assembly (or nuget) by referencing a type in the assembly.
        // See http://stackoverflow.com/a/22315164/1141360.
        private static void EnsureStaticReference<T>()
        {
            var dummy = typeof(T);
        }
    }
}
