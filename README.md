SportsStore application from "Pro ASP.NET MVC 3 Framework" book ch7-ch9
===
### A project made for studying purposes. Implemented with ASP.NET MVC 5. Is not exact copy of the original.

To open admin page use relative url /Admin/Index

#### Interesting features
* SportsStore.Domain.MissingDllHack - ensuring that required library is copied to output folder by explicitly referencing type from it
* SportsStore.WebUI.Binders.CartModelBinder - binding model from Session