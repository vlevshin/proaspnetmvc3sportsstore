﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SportsStore.Domain.Abstract;
using SportsStore.Domain.Entities;
using SportsStore.WebUI.Controllers;

namespace SportsStore.UnitTests
{
    [TestClass]
    public class AdminControllerDeleteTest
    {
        [TestMethod]
        public void Can_Delete_Valid_Products()
        {
            // Arrange
            var prod = new Product { ProductID = 2, Name = "Test" };
            var mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new Product[] {
                new Product {ProductID = 1, Name = "P1"},
                prod,
                new Product {ProductID = 3, Name = "P3"},
            }.AsQueryable());

            var target = new AdminController(mock.Object);

            // Act
            target.Delete(prod.ProductID);

            // Assert - ensure that the repository delete method was 
            // called with the correct Product 
            mock.Verify(m => m.DeleteProduct(prod));
        }

        [TestMethod]
        public void Cannot_Delete_Invalid_Products()
        {
            // Arrange
            var mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new Product[] {
                new Product {ProductID = 1, Name = "P1"},
                new Product {ProductID = 2, Name = "P2"},
                new Product {ProductID = 3, Name = "P3"},
            }.AsQueryable());

            var target = new AdminController(mock.Object);

            // Act - delete using an ID that doesn't exist
            target.Delete(100);

            // Assert - ensure that the repository delete method was never called
            mock.Verify(m => m.DeleteProduct(It.IsAny<Product>()), Times.Never());
        }
    }
}
