﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Ninject;
using SportsStore.Domain.Abstract;
using SportsStore.Domain.Entities;
using SportsStore.WebUI.Controllers;
using SportsStore.WebUI.Models;

namespace SportsStore.UnitTests
{
    [TestClass]
    public class ProductControllerTest
    {
        private Product[] products;
        private IKernel ninjectKernel;

        private int totalItems;

        [TestInitialize]
        public void TestInitialize()
        {
            products = new Product[]
            {
                new Product { ProductID = 1, Name = "P1", Category ="Cat1" },
                new Product { ProductID = 2, Name = "P2", Category ="Cat2" },
                new Product { ProductID = 3, Name = "P3", Category ="Cat1" },
                new Product { ProductID = 4, Name = "P4", Category ="Cat2" },
                new Product { ProductID = 5, Name = "P5", Category ="Cat3" },
            };

            totalItems = products.Length;

            ninjectKernel = new StandardKernel();

            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(products.AsQueryable());

            ninjectKernel.Bind<IProductRepository>().ToConstant(mock.Object);
        }

        [TestMethod]
        public void List_Can_Paginate()
        {
            // arrange
            ProductController controller = ninjectKernel.Get<ProductController>();
            controller.PageSize = 3;

            // act
            IEnumerable<Product> result = ((ProductListViewModel)controller.List(null, 2).Model).Products;

            // assert
            Product[] prodArray = result.ToArray();
            Assert.IsTrue(prodArray.Length == 2);
            Assert.AreEqual(prodArray[0].Name, products[3].Name);
            Assert.AreEqual(prodArray[1].Name, products[4].Name);
        }

        [TestMethod]
        public void List_SendsCorrectPagingInfo()
        {
            // arrange
            ProductController controller = ninjectKernel.Get<ProductController>();
            const int itemsPerPage = 3;
            controller.PageSize = itemsPerPage;

            // act
            const int currentPage = 2;
            var result = (ProductListViewModel)controller.List(null, currentPage).Model;

            // assert
            PagingInfo pagingInfo = result.PagingInfo;
            Assert.AreEqual(pagingInfo.CurrentPage, currentPage);
            Assert.AreEqual(pagingInfo.ItemsPerPage, itemsPerPage);
            Assert.AreEqual(pagingInfo.TotalItems, totalItems);
            Assert.AreEqual(pagingInfo.TotalPages, 2);
        }

        [TestMethod]
        public void List_WithCategoryParam_FiltersByCategory()
        {
            // arrange
            ProductController controller = ninjectKernel.Get<ProductController>();
            const int itemsPerPage = 3;
            controller.PageSize = itemsPerPage;

            // act
            Product[] result =
                ((ProductListViewModel)controller.List("Cat2", 1).Model).Products.ToArray();

            // assert
            Assert.AreEqual(result.Length, 2);
            Assert.IsTrue(result[0].Name == "P2" && result[0].Category == "Cat2");
            Assert.IsTrue(result[1].Name == "P4" && result[0].Category == "Cat2");
        }

        [TestMethod]
        public void Generate_Category_Specific_Product_Count()
        {
            // Arrange
            ProductController target = ninjectKernel.Get<ProductController>();
            target.PageSize = 3;

            // Action
            int res1 = ((ProductListViewModel)target.List("Cat1").Model).PagingInfo.TotalItems;
            int res2 = ((ProductListViewModel)target.List("Cat2").Model).PagingInfo.TotalItems;
            int res3 = ((ProductListViewModel)target.List("Cat3").Model).PagingInfo.TotalItems;
            int resAll = ((ProductListViewModel)target.List(null).Model).PagingInfo.TotalItems;

            // Assert
            Assert.AreEqual(res1, 2);
            Assert.AreEqual(res2, 2);
            Assert.AreEqual(res3, 1);
            Assert.AreEqual(resAll, 5);
        }
    }
}
